CFLAGS = -std=gnu11 -Wall -Werror -Wextra
CC = gcc

.PHONY: all clean

all: factory

clean:
	$(RM) factory

factory: factory.c
	$(CC) $(CFLAGS) $^ -lm -o $@ -lrt -pthread
