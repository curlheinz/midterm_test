#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

// surce: exercise06 task2
#define NUM_OF_PTHREADS 20

static pthread_mutex_t knob_lock;
static pthread_mutex_t door_lock;
//source exxercise07 task 2
pthread_cond_t cv;

int knobs = 0;
int total_produced_knobs = 0;
int doors = 0;

pthread_t threadID[NUM_OF_PTHREADS];

void *threadFunc(void *arg){
	int *worker = (int *)arg;
	unsigned seed = (unsigned)pthread_self();
	int lazines = rand_r(&seed) % 2;
	while (1){
		if (*worker == 1){ /* creates knob*/
			usleep((10 + lazines * 30) * 1000);
			pthread_mutex_lock(&knob_lock);
			knobs++;
			total_produced_knobs++;
			if (pthread_cond_signal(&cv) != 0){
				perror("cond_signal failed");
			}
			pthread_mutex_unlock(&knob_lock);
		}
		else{ /* creates doors*/
			pthread_mutex_lock(&knob_lock);
			while (knobs <= 0){
				if (pthread_cond_wait(&cv, &knob_lock) != 0){
					perror("cond_wait failed");
				}
			}
			knobs--;
			pthread_mutex_unlock(&knob_lock);
			usleep((10 + lazines * 90) * 1000);
			pthread_mutex_lock(&door_lock);
			doors++;
			pthread_mutex_unlock(&door_lock);
		}
	}

	pthread_exit(0);
}

void init_worker(int worker[]){
	for (int i = 0; i < NUM_OF_PTHREADS / 2; i++){
		worker[i] = 1;
		worker[i + 10] = -1;
	}
	return;
}

void change_worker(int worker[], int to_change){
	if (to_change == 1){
		for (int i = 0; i < 20; i++){
			if (worker[i] == -1){
				worker[i] = 1;
				break;
			}
		}
	}
	else{
		for (int i = 0; i < 20; i++){
			if (worker[i] == 1){
				worker[i] = -1;
				break;
			}
		}
	}
	int working_on_doors = 0;
	int working_on_knobs = 0;
	for (int i = 0; i < 20; i++){
		if (worker[i] == 1){
			working_on_knobs++;
		}
		else{
			working_on_doors++;
		}
	}
	printf("  Workers reassigned: %d making knobs, %d making doors\n", working_on_knobs, working_on_doors);
}

int main(int argc, char **argv){
	if (argc != 3){
		printf("to many or to few arguments\n");
	}
	int enable_load_balancing = atoi(argv[1]);
	int doors_to_make = atoi(argv[2]);
	pthread_mutex_init(&knob_lock, NULL);
	pthread_mutex_init(&door_lock, NULL);
	int worker[NUM_OF_PTHREADS];
	init_worker(worker);
	time_t start_t, end_t;
	time(&start_t);
	for (int i = 0; i < NUM_OF_PTHREADS; i++){
		if (pthread_create(&threadID[i], NULL, threadFunc, &worker[i])){
			perror("Error creating thread\n");
			return 1;
		}
	}
	while (doors < doors_to_make){
		sleep(1);
		time(&end_t);
		float trouput_doors = doors / difftime(end_t, start_t);
		float trouput_knobs = total_produced_knobs / difftime(end_t, start_t);
		printf("producing %.00f/s knobs and %.00f/s doors\n", trouput_knobs, trouput_doors);
		if (enable_load_balancing == 1){
			if (trouput_doors * 1.1 < trouput_knobs){
				change_worker(worker, -1);
			}
			else if (trouput_doors > trouput_knobs * 1.1){
				change_worker(worker, 1);
			}
		}
	}

	for (int i = 0; i < NUM_OF_PTHREADS; i++){
		pthread_cancel(threadID[i]);
	}
	pthread_mutex_destroy(&knob_lock);
	pthread_mutex_destroy(&door_lock);
	return EXIT_SUCCESS;
}